﻿using LigamagicApi.Utils;
using LigamagicBotConsole.Responses;
using MtgApiManager.Lib.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigamagicBotConsole.Utils
{
    public static class PriceGetter
    {
        public static string GetCardPrices(string card)
        {
            var httpClient = new HttpClient();

            var jsonContent = $"{{\"Cards\":[{{\"Name\":\"{card}\",\"Set\":\"\",\"Copies\":1}}]}}";

            var response = httpClient.PostJson(ConfigurationManager.AppSettings["ligamagicApi"], jsonContent);

            if (!response.Contains("["))
                return $"Nenhuma loja está vendendo {card} :/";

            return FormatResponseMessage(response, card);
        }

        private static string FormatResponseMessage(string response, string card)
        {
            var ligamagicResponses = GetLigamagicObjectFromJson(response);

            var message = new StringBuilder($"{card} foi encontrado(a) em {ligamagicResponses.Count} lojas, listadas abaixo em preço ascendente:");

            foreach (var ligamagicResponse in ligamagicResponses.OrderBy(x => x.Cards.OrderBy(y => y.Price).FirstOrDefault().Price))
            {
                message.AppendLine();
                message.AppendLine($"{ligamagicResponse.Store.Name}: R${ligamagicResponse.Cards.OrderBy(x => x.Price).FirstOrDefault().Price.ToString("0.00")}");
            }

            return message.ToString();
        }

        private static List<LigamagicApiResponse> GetLigamagicObjectFromJson(string response)
        {
            var jArray = JArray.Parse(response);
            return jArray.ToObject<List<LigamagicApiResponse>>();
        }
    }
}
