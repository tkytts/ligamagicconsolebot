﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace LigamagicApi.Utils
{
    public class HttpClient
    {
        public string Get(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html; charset=utf-8";
            request.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            var response = (HttpWebResponse)request.GetResponse();

            using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8, true))
            {
                return WebUtility.HtmlDecode(reader.ReadToEnd());
            }
        }

        public string PostJson(string url, string content)
        {
            var request = WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "POST";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(content);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var response = (HttpWebResponse)request.GetResponse();

            using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8, true))
            {
                return WebUtility.HtmlDecode(reader.ReadToEnd());
            }
            
        }
    }
}