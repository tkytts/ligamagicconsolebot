﻿using System;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Args;
using MtgApiManager.Lib.Service;
using MtgApiManager.Lib.Model;
using LigamagicBotConsole.Utils;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace LigamagicBot.Bots
{
    public class LmgBot
    {
        public LmgBot()
        {
            BotInterface = new TelegramBotClient(ConfigurationManager.AppSettings["ligamagicToken"]);

            BotInterface.OnMessage += BotInterface_OnMessage;

            BotInterface.StartReceiving();
        }

        public TelegramBotClient BotInterface { get; set; }

        private async void BotInterface_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            var chatId = e.Message.Chat.Id;

            if (e.Message.Type.Equals(MessageType.TextMessage))
            {
                if (this.IsMessageStart(e.Message.Text))
                    await BotInterface.SendTextMessageAsync(chatId, "Bem-vindo, por favor me diga uma carta para a consulta de preços.");
                else
                    this.SearchForCard(chatId, e.Message.Text);
            }
            else
            {
                await BotInterface.SendTextMessageAsync(chatId, "Desculpe, eu só trabalho com mensagens de texto!");
            }
        }

        private bool IsMessageStart(string message)
        {
            return message.ToLower().Contains("/start");
        }

        private async void SearchForCard(long chatId, string text)
        {
            this.SendCardPrices(chatId, text);
        }

        private async void SendCardPrices(long chatId, string card)
        {
            await BotInterface.SendTextMessageAsync(chatId, PriceGetter.GetCardPrices(card));
        }

        private async void SendFountManyCardsMessage(long chatId, IEnumerable<Card> foundCards)
        {
            var response = "Parece que a sua busca retornou mais de uma carta, por favor escolha uma das seguintes opções.";

            var cardsArray = foundCards.ToArray();

            var rkm = new ReplyKeyboardMarkup();
            var rows = new List<KeyboardButton[]>();
            var cols = new List<KeyboardButton>();
            for (var i = 1; i < cardsArray.Length; i++)
            {
                cols.Add(new KeyboardButton(cardsArray[i].Name));
                if (i % 4 != 0) continue;
                rows.Add(cols.ToArray());
                cols = new List<KeyboardButton>();
            }
            rkm.Keyboard = rows.ToArray();

            await BotInterface.SendTextMessageAsync(
            chatId,
            response,
            replyMarkup: rkm);
        }

        public string GetBotInfo()
        {
            var response = BotInterface.GetMeAsync().Result;

            return response.ToString();
        }
    }

    public class CardComparer : IEqualityComparer<Card>
    {
        public bool Equals(Card x, Card y)
        {
            return x.Name.Equals(y.Name);
        }

        public int GetHashCode(Card obj)
        {
            return base.GetHashCode();
        }
    }

}
