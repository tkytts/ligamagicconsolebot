﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigamagicBotConsole.Responses
{
    public class LigamagicApiResponse
    {
            public StoreResponse Store { get; set; }
            public List<CardResponse> Cards { get; set; }
    }
}
