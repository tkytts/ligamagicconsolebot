﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigamagicBotConsole.Responses
{
    public class CardResponse
    {
        public string Name { get; set; }
        public string Set { get; set; }
        public float Price { get; set; }
        public int Copies { get; set; }
    }
}
